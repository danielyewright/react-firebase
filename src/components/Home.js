import React from 'react';
import firebaseAuth from '../firebase';

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.logout = this.logout.bind(this);
  }

  logout() {
    firebaseAuth.auth().signOut();
  }

  render() {
    return (
      <div className="container mt-5">
        <div className="jumbotron">
          <h1 className="display-4">Hello, world!</h1>
          <p className="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
          <hr className="my-4" />
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <button type="button" className="btn btn-primary btn-lg" onClick={this.logout}>Logout</button>
        </div>
      </div>
    );
  }
}

export default Home;