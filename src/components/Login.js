import React from 'react';
import firebaseAuth from '../firebase';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);
    this.signup = this.signup.bind(this);
  }

  login(e) {
    e.preventDefault();
    firebaseAuth.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {
        // ...
      })
      .catch(error => {
        console.log(error);
      });
  }

  signup(e){
    e.preventDefault();
    firebaseAuth.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
      .then(u => {
        // ...
      }).then(u => {
        console.log(u)
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    return (
      <div className="col-sm-4 mx-auto py-5">
        <h1>Please Login</h1>
        <form>
          <div className="form-group">
            <label>Email address</label>
            <input type="email" className="form-control" name="email" value={this.state.email} onChange={this.handleChange} placeholder="Email" required />
          </div>
          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" name="password" value={this.state.password} onChange={this.handleChange} placeholder="Password" required />
          </div>
          <div className="mt-3">
            <button type="submit" onClick={this.login} className="btn btn-primary mr-1">Login</button>
            <button type="button" onClick={this.signup} className="btn btn-secondary">Signup</button>
          </div>
        </form>
      </div>
    );
  }
}

export default Login;