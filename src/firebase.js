import * as firebase from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyAE2wfj6cspg7F9s2gC7XpVt5CPPorT5c8",
  authDomain: "react-firebase-833ea.firebaseapp.com",
  databaseURL: "https://react-firebase-833ea.firebaseio.com",
  projectId: "react-firebase-833ea",
  storageBucket: "react-firebase-833ea.appspot.com",
  messagingSenderId: "92634934248",
  appId: "1:92634934248:web:17ca7fc19fa22165"
};

const firebaseAuth = firebase.initializeApp(config);
export default firebaseAuth;